npm init --yes
npm i --save @hapi/boom
npm i --save @hapi/hapi
npm i --save @hapi/inert
npm i --save @hapi/joi
npm i --save @hapi/vision
npm i --save hapi-auth-jwt2
npm i --save hapi-swagger
npm i --save jwks-rsa
npm i --save pg
npm i --save pg-hstore
npm i --save sequelize
npm i --save sequelize-cli

npm i --save-dev @hapi/code
npm i --save-dev gulp
npm i --save-dev gulp-lab
npm i --save-dev @hapi/lab
