'use strict';
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('Boards', {
      id: {
        allowNull: false,
        primaryKey: true,
        type: Sequelize.UUID
      },
      AuthorId: {
        type: Sequelize.UUID,
        references: { model: 'Accounts', key: 'id' }
      },
      name: {
        type: Sequelize.STRING
      },
      airtableRecordId: {
        type: Sequelize.STRING
      },
      terms: {
        type: Sequelize.JSONB
      },
      saves: {
        type: Sequelize.JSONB
      },
      searches: {
        type: Sequelize.JSONB
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('Boards');
  }
};
