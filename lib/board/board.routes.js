module.exports = {
  routes: (models) => {
    const helpers = require('../helpers')(models);
    const controllers = require('./board.controllers')(models);
    const boardModels = require('./board.models');
    return [
      {
        method: 'GET',
        path: '/boards',
        handler: controllers.getUserBoards,
        options: {
          auth: 'jwt',
          pre: [
            { method: helpers.ensureAccount, assign: 'account', failAction: 'error' },
          ],
          description: 'Get Boards for Current User',
          notes: 'Get Boards associated with the current account from the Database',
          tags: ['api', 'Boards'],
        }
      },
      {
        method: 'GET',
        path: '/boards/{boardId}',
        handler: controllers.getBoard,
        options: {
          auth: 'jwt',
          validate: {
            params: boardModels.id,
          },
          pre: [
            { method: helpers.ensureAccount, assign: 'account', failAction: 'error' },
            { method: helpers.ensureBoard, assign: 'board', failAction: 'error' },
            { method: helpers.ensureBoardAuthorOrAdmin, failAction: 'error' },
          ],
          description: 'Get A Single Board',
          notes: 'Get a Board from the database by the boardId',
          tags: ['api', 'Boards'],
        }
      },
      {
        method: 'POST',
        path: '/boards',
        handler: controllers.postBoard,
        options: {
          auth: 'jwt',
          validate: {
            payload: boardModels.postBoard,
          },
          pre: [
            { method: helpers.ensureAccount, assign: 'account', failAction: 'error' },
          ],
          description: 'Add a Board to the Database',
          notes: 'Add a Board associated with the current account to the Database',
          tags: ['api', 'Boards'],
        }
      },
      {
        method: 'PATCH',
        path: '/boards/{boardId}',
        handler: controllers.patchBoard,
        options: {
          auth: 'jwt',
          validate: {
            params: boardModels.id,
            payload: boardModels.patchBoard,
          },
          pre: [
            { method: helpers.ensureAccount, assign: 'account', failAction: 'error' },
            { method: helpers.ensureBoard, assign: 'board', failAction: 'error' },
            { method: helpers.ensureBoardAuthorOrAdmin, failAction: 'error' },
          ],
          description: 'Edit a Board to the Database',
          notes: 'Edit a Board associated with the current account to the Database',
          tags: ['api', 'Boards'],
        }
      },

    ];
  },
};
