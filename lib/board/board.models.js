'use strict';
const { Model } = require('sequelize');
const Joi = require('@hapi/joi');

// To be expanded later
const joiCorpObject = Joi.object();
let joiPropertyObject = Joi.object();

const joiSearchObject = Joi.object().keys({
  query: Joi.object(),
  count: Joi.number(),
  notices: Joi.array().items(
    Joi.string(),
  ),
  results: Joi.array().items(
    joiCorpObject,
    joiPropertyObject,
  ),
  type: Joi.string(),
});

const boardModelsExport = {
  db: (sequelize, DataTypes) => {
    class Board extends Model {
      /**
       * Helper method for defining associations.
       * This method is not a part of Sequelize lifecycle.
       * The `models/index` file will call this method automatically.
       */
      static associate(models) {
        // define association here
        models.Board.belongsTo(models.Account, {as: 'Author'});
      }
    };
    Board.init({
      id: {
        type: DataTypes.UUID,
        defaultValue: DataTypes.UUIDV4,
        primaryKey: true,
      },
      name: DataTypes.STRING,
      airtableRecordId: DataTypes.STRING,
      terms: {
        type: DataTypes.JSONB,
        defaultValue: {},
      },
      saves: {
        type: DataTypes.JSONB,
        defaultValue: [],
      },
      searches: {
        type: DataTypes.JSONB,
        defaultValue: [],
      },
    }, {
      sequelize,
      modelName: 'Board',
    });
    return Board;
  },
  id: Joi.object().keys({
    boardId: Joi.string().guid().required(),
  }),
  postBoard: Joi.object().keys({
    name: Joi.string().required(),
    airtableRecordId: Joi.string(),
    terms: Joi.object(),
    saves: Joi.array(),
    searches: Joi.array().items(
      joiSearchObject
    ),
  }),
  patchBoard: Joi.object().keys({
    name: Joi.string(),
    airtableRecordId: Joi.string(),
    terms: Joi.object(),
    saves: Joi.array().items(
      joiCorpObject,
      joiPropertyObject,
    ),
    searches: Joi.array().items(
      // Search Object (meta about search and the results)
      joiSearchObject,
    ),
  }),
};

module.exports = boardModelsExport;
