module.exports = (models) => {
  const Boom = require('@hapi/boom');

  return {
    getUserBoards: async function(request, h) {
      let account = request.pre.account;
      let boards;
      try {
        boards = getBoardsByUserId(account.id);
      } catch (e) {
        throw Boom.badImplementation('Error during getBoardsByUserId(account.id). ' + e);
      }
      return boards;
    },
    getBoard: async function(request, h) {
      let board = request.pre.board;
      return board;
    },
    postBoard: async function(request, h) {
      let account = request.pre.account;
      let payload = request.payload;

      let boardInfo = {
        name: payload.name,
        AuthorId: account.id,
      };

      if (payload.hasOwnProperty('airtableRecordId')) {
        boardInfo.airtableRecordId = payload.airtableRecordId;
      }
      if (payload.hasOwnProperty('terms')) {
        boardInfo.terms = payload.terms;
      }
      if (payload.hasOwnProperty('saves')) {
        boardInfo.saves = payload.saves;
      }
      if (payload.hasOwnProperty('searches')) {
        boardInfo.searches = payload.searches;
      }

      let boardInstance;
      try {
        boardInstance = await createBoard(boardInfo);
      } catch (e) {
        throw Boom.badImplementation('Error during createBoard(boardInfo). ' + e);
      }

      return boardInstance;
    },
    patchBoard: async function(request, h) {
      let account = request.pre.account;
      let board = request.pre.board;
      let payload = request.payload;

      let boardInfo = {};

      if (payload.hasOwnProperty('name')) {
        boardInfo.name = payload.name;
      }

      // TODO: support changing AccountId if being PATCHed by Admin

      if (payload.hasOwnProperty('airtableRecordId')) {
        boardInfo.airtableRecordId = payload.airtableRecordId;
      }
      if (payload.hasOwnProperty('terms')) {
        boardInfo.terms = payload.terms;
      }
      if (payload.hasOwnProperty('saves')) {
        boardInfo.saves = payload.saves;
      }
      if (payload.hasOwnProperty('searches')) {
        boardInfo.searches = payload.searches;
      }

      let boardInstance;
      try {
        boardInstance = await board.update(boardInfo);
      } catch (e) {
        throw Boom.badImplementation('Error during board.update(boardInfo). ' + e);
      }

      return boardInstance;
    },
    lib: {
      getBoardByBoardId: getBoardByBoardId,
    },
  };

  function getBoardsByUserId(userId) {
    return models.Board.findAll({
      where: {
        AuthorId: userId,
      },
      order: [
        ['createdAt', 'ASC']
      ]
    });
  }

  function createBoard(boardInfo) {
    return models.Board.create(boardInfo);
  }

  function getBoardByBoardId(boardId) {
    return models.Board.findByPk(boardId);
  }

};
