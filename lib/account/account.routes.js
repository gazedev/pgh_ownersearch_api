module.exports = {
  routes: (models) => {
    const controllers = require('./account.controllers')(models);
    const accountModels = require('./account.models');
    return [
      {
        method: 'GET',
        path: '/accounts',
        handler: controllers.getAccount,
        config: {
          auth: 'jwt',
          description: 'Get Current Authentication Account',
          notes: 'Get the account associated with the current authentication',
          tags: ['api', 'Accounts'],
        }
      },
      {
        method: 'POST',
        path: '/accounts',
        handler: controllers.postAccount,
        options: {
          auth: 'jwt',
          description: 'Create Account',
          notes: 'create a account and receive the account object in return',
          tags: ['api', 'Accounts'],
        },
      },
    ];
  },
};
