module.exports = (models) => {
  const Boom = require('@hapi/boom');

  return {
    getAccount: async function(request, h) {
      let externalId = request.auth.credentials.subjectId;
      let account;
      try {
        account = await getAccount(request.auth.credentials);
      } catch (e) {
        throw Boom.badImplementation('Error during getAccount(request.auth.credentials). ' + e);
      }

      if (account === null) {
        throw Boom.notFound("No account found for the provided credentials.");
      }

      return account;
    },
    postAccount: async function(request, h) {
      let credentials = request.auth.credentials;

      let account;
      try {
        account = await getAccount(credentials);
      } catch (e) {
        throw Boom.badImplementation('Error during postAccount - getAccount(credentials). ' + e);
      }

      if (account !== null) {
        throw Boom.conflict("Account already exists for provided credentials");
      }

      return models.Account.create({
        externalId: credentials.subjectId,
      })
      .then(response => {
        return response;
      })
      .catch(error => {
        return error;
      });
    },
    lib: {
      getAccount: getAccount,
      ensureAccount: ensureAccount,
    },
  };

  // getCurrentAccount based off auth credentials
  function getAccount(credentials) {
    return models.Account.findOne({
      where: {
        externalId: credentials.subjectId,
      }
    })
  }

  function getAccountById(accountId) {
    return models.Account.findByPk(accountId);
  }

  async function ensureAccount(request) {
    let account;
    try {
      account = await getAccount(request.auth.credentials);
    } catch (e) {
      throw Boom.badImplementation('Error during getAccount(request.auth.credentials). ' + e);
    }

    if (account === null) {
      throw Boom.badRequest('Must have an Account');
    }

    return account;
  }

  function isAdmin(request) {
    if (request.auth.credentials.subjectId === process.env.SUPER_ADMIN) {
      return true;
    }
    return false;
  }


};
