'use strict';
const {
  Model
} = require('sequelize');
module.exports = {
  db: (sequelize, DataTypes) => {
    class Account extends Model {
      /**
       * Helper method for defining associations.
       * This method is not a part of Sequelize lifecycle.
       * The `models/index` file will call this method automatically.
       */
      static associate(models) {
        // define association here
      }
    };
    Account.init({
      id: {
        type: DataTypes.UUID,
        defaultValue: DataTypes.UUIDV4,
        primaryKey: true
      },
      externalId: DataTypes.STRING,
      // userData should only be exchanged between the api and the user's client
      userData: {
        type: DataTypes.JSONB,
        defaultValue: {},
      },
    }, {
      sequelize,
      modelName: 'Account',
    });
    return Account;
  }
};
