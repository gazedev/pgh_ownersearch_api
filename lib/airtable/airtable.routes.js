module.exports = {
  routes: (models) => {
    const Joi = require('@hapi/joi');
    const controllers = require('./airtable.controllers')(models);

    return [
      {
        method: 'GET',
        path: '/airtable/plaintiff-clusters',
        handler: controllers.getPlaintiffClusters,
        options: {
          auth: 'jwt',
          description: 'Get Plaintiff Clusters',
          notes: 'Get the plaintiff clusters from airtable and display results.',
          tags: ['api', 'Airtable'],
        }
      },
    ];
  },
};
