module.exports = (models) => {
  const Boom = require('@hapi/boom');

  return {

    getPlaintiffClusters: async function(request, h) {
      let results;
      try {
        results = getPlaintiffClustersRecords();
      } catch (e) {
        console.error('Error with getPlaintiffClustersRecords', e);
      }
      return results;
    },

  };

  async function getPlaintiffClustersRecords() {
    var Airtable = require('airtable');
    var base = new Airtable({apiKey: process.env.AIRTABLE_API_KEY}).base('appnrQHZQT7F0A86Z');
    let records = await base('MDJ Plaintiff Clusters').select({
        view: "Grid view"
    }).all();
    results = records.map(record => {
      let result = record.fields;
      result.id = record.getId();
      return result;
    });
    return results;
  }

};
