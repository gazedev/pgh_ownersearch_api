const Boom = require('@hapi/boom');

module.exports = (models) => {
  let accountLib = require('./account/account.controllers.js')(models).lib;
  let boardLib = require('./board/board.controllers.js')(models).lib;
  return {
    ensureAdmin: async function(request) {
      if (
        // if SUPER_ADMIN is undefined or empty
        !process.env.SUPER_ADMIN ||
        request.auth.credentials.subjectId !== process.env.SUPER_ADMIN
      ) {
        throw Boom.forbidden('Must be an Admin');
      }
      return true;
    },
    ensureAccount: async function(request) {
      let account;
      try {
        account = await accountLib.getAccount(request.auth.credentials);
      } catch (e) {
        throw Boom.badImplementation('Error during getAccount(request.auth.credentials). ' + e);
      }

      if (account === null) {
        throw Boom.badRequest('Must have an Account');
      }

      return account;
    },
    ensureBoard: async function(request) {
      let board;
      try {
        board = await boardLib.getBoardByBoardId(request.params.boardId);
      } catch (e) {
        throw Boom.badImplementation('Error during getBoardByBoardId(request.params.boardId). ' + e);
      }

      if (board === null) {
        throw Boom.notFound('No Board with that id found');
      }

      return board;
    },
    ensureBoardAuthorOrAdmin: async function(request) {
      // For this function to work, board and account need to be loaded into pre
      let board = request.pre.board;
      let account = request.pre.account;

      if (
        board.AuthorId !== account.id &&
        request.auth.credentials.subjectId !== process.env.SUPER_ADMIN
      ) {
        throw Boom.forbidden('Board.AuthorId must match your Account ID or you must be a Super Admin');
      }

      return true;
    }
  };
};
